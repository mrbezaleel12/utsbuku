<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class transaksiCont extends Controller
{
    public function index() {
        $transaksi = DB::table('transaksi')->get();

        return view ('pages.transaksi', ['tr'=>$transaksi]);
    }

    public function addTransaksi() {
        $mahasiswa = DB::table('mahasiswa')->get();
        $buku = DB::table('buku')->get();
        return view('pages.addTransaksi', 
            [
                'mahasiswa' => $mahasiswa, 
                'buku' => $buku
            ]
        );
    }

    public function createTransaksi( Request $req) {
        $idMhs = $req->idMhs;
        $idBuku = $req->idBuku;
        $tanggal_pinjam = $req->tanggal_pinjam;
        $status_pinjam = '1';
        $total_biaya = '0';

        DB::table('transaksi')->insert([
            'id_mahasiswa' => $idMhs,
            'id_buku' => $idBuku,
            'tanggal_pinjam' => $tanggal_pinjam,                  
            'status_pinjam' => $status_pinjam,
            'total_biaya' => $total_biaya
        ]);
        DB::table('buku')->where('id', $req->id_buku)->decrement('stok_buku');
        return redirect ('transaksi')->with('success', 'Data berhasil ditambahkan.');

    }
    
    public function editTransaksi($id) {
        $transaksi = DB::table('transaksi')->find($id);
        $mahasiswa = DB::table('mahasiswa')->get();
        $buku = DB::table('buku')->get();
        return view('pages.editTransaksi', 
            [
                'mahasiswa' => $mahasiswa, 
                'buku' => $buku,
                'transaksi' => $transaksi
            ]
        );

        return view ('pages.editTransaksi', compact('transaksi'));
        
    }

    public function updateTransaksi(Request $req, $id) {
        $idMhs = $req->idMhs;
        $idBuku = $req->idBuku;
        $tanggal_pinjam = $req->tanggal_pinjam;
        $tanggal_kembali = $req->tanggal_kembali;
        $status_pinjam = $req->status_pinjam;
        $total_biaya = $req->total_biaya;

        DB::table('transaksi')->update([
            'id_mahasiswa' => $idMhs,
            'id_buku' => $idBuku,
            'tanggal_pinjam' => $tanggal_pinjam,  
            'tanggal_kembali' => $tanggal_kembali,                
            'status_pinjam' => $status_pinjam,
            'total_biaya' => $total_biaya
        ]);
        
        return redirect ('transaksi')->with('success', 'Data berhasil diubah.');
    }

    public function deleteTransaksi($id) 
    {
        DB::table('transaksi')->where('id', $id)->delete();
        return redirect('/transaksi')->with('success', 'Data berhasil dihapus.');
    }
}
