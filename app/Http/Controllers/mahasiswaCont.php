<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class mahasiswaCont extends Controller
{
    public function index() {
        $mahasiswa = DB::table('mahasiswa')->get();

        return view ('pages.mahasiswa', ['mhs'=>$mahasiswa]);
    }

    public function addMhs() {
        return view ('pages.addMahasiswa');
    }

    public function create(Request $req) {
        $nama = $req->nama;
        $nim = $req->nim;
        $email = $req->email;
        $no_telp = $req->no_telp;
        $prodi = $req->prodi;
        $jurusan = $req->jurusan;
        $fakultas = $req->fakultas;

        DB::table('mahasiswa')->insert([
            'nama' => $nama,
            'nim' => $nim,
            'email' => $email,
            'no_telp' => $no_telp,
            'prodi' => $prodi,
            'jurusan' => $jurusan,
            'fakultas' => $fakultas
        ]);

        return redirect ('mahasiswa')->with('success', 'Data berhasil ditambahkan.');
    }

    public function editMhs($id){
        $mahasiswa = DB::table('mahasiswa')->find($id);

        return view ('pages.editMahasiswa', compact('mahasiswa'));
    }

    public function updateMhs(Request $req, $id){
        $nama = $req->nama;
        $nim = $req->nim;
        $email = $req->email;
        $no_telp = $req->no_telp;
        $prodi = $req->prodi;
        $jurusan = $req->jurusan;
        $fakultas = $req->fakultas;

        DB::table('mahasiswa')->where('id', $id)->update([
            'nama' => $nama,
            'nim' => $nim,
            'email' => $email,
            'no_telp' => $no_telp,
            'prodi' => $prodi,
            'jurusan' => $jurusan,
            'fakultas' => $fakultas
        ]);
        return redirect ('mahasiswa')->with('success', 'Data berhasil diubah.');
    }

    public function delete($id)
    {
        DB::table('mahasiswa')->where('id', $id)->delete();
        return redirect('/mahasiswa')->with('success', 'Data berhasil dihapus.');
    }

}
