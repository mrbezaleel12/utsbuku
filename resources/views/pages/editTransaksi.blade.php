@extends('layout.layout')
@section('title')
    Update Data Transaksi
@endsection
@section('header')
    Manajemen Data Transaksi
@endsection
@section('subheader')
    Update Data
@endsection
@section('content')
<div class="mb-5">
    <div class="container col-7 p-2">  
        <div class="card p-1">
            <div class="card-body">
                <h4 class="text-dark fw-bold text-center">Edit Data Transaksi</h4>
                <form action="/updateTransaksi/{{$transaksi->id}}" method="post" enctype="multipart/form">
                {{ csrf_field() }}
                <div class="row mb-3">
                    <div class="col">
                        <label for="idMhs" class="form-label">Nama Mahasiswa</label>
                        <select class="form-select" name="idMhs" id="idMhs" required>
                            <option selected disabled>Select</option>
                            @foreach ($mahasiswa as $m)
                                <option value="{{ $m->id }}">{{ $m->nama}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col">
                        <label for="idBuku" class="form-label">Judul Buku</label>
                        <select class="form-select" name="idBuku" id="idBuku" required>
                            <option selected disabled>Select</option>
                            @foreach ($buku as $b)
                                <option value="{{ $b->id }}">{{ $b->judul_buku}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="mb-3">
                    <label for="tanggal_pinjam" class="form-label">Tanggal Pinjam</label>
                    <input type="date" name="tanggal_pinjam" value="{{ date($transaksi->tanggal_pinjam) }}"class="form-control" id="tanggal_pinjam" readonly>
                </div>
                <div class="mb-3">
                    <label for="tanggal_kembali" class="form-label">Tanggal Kembali</label>
                    <input type="date" name="tanggal_kembali" class="form-control" id="tanggal_kembali" required>
                </div>
                <div class="mb-3">
                    <label for="status_pinjam" class="form-label">Status Pinjam</label>
                        <select class="form-select" name="status_pinjam" id="status_pinjam" required>
                            <option selected disabled>Select</option>
                                <option value="0">dikembalikan</option>
                                <option value="1"> dipinjam</option>
                        </select>
                </div>
                <button type="submit" class="btn btn-primary me-2">Update</button>
                <div class="btn-group btn-group" role="group" aria-label="Third group">
                    <a href="/transaksi" class="btn btn-danger">Cancel</a>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
